#include <SoftwareSerial.h>
#include <Music.h>

uint32_t BPM = 100;       // Tempo
uint32_t Q = 60000 / BPM; // quarter note
uint32_t H = 2 * Q;      // half note
uint32_t W = 4 * Q;      // whole note
uint32_t E = Q / 2;      // eighth note
uint32_t S = Q / 4;      // sixteenth note
uint32_t T = Q / 8;      // thirty second note
uint32_t ST = S + T;     // sixteenth thirty second note

float     conorNotes[] = {G5, G5, G5, Eb5, Eb5, Eb5, Ab5, Ab5, Ab5, Bb5, Bb5, Bb5, Eb6};
uint32_t  conorBeats[] = {E,  S,  S,  E,   S,   S,   E,   S,   S,   E,   S,   S,   Q + E};
uint16_t  conorLength  = sizeof(conorNotes) / sizeof(conorNotes[0]);
float conorTempoFactor = 1.0 / 1.7;

float     michelleNotes[] = {C4, REST, Eb4, REST, C4, Gb4, G4};
uint32_t  michelleBeats[] = {E,  S,    E,   S,    ST, E,   E};
uint16_t  michelleLength  = sizeof(michelleNotes) / sizeof(michelleNotes[0]);
float michelleTempoFactor = 1.0 / 2.0;

float     deniedNotes[] = {B6, Gb6, B5, Gb6};
uint32_t  deniedBeats[] = {E,  E,   S,  Q};
uint16_t  deniedLength  = sizeof(deniedNotes) / sizeof(deniedNotes[0]);
float deniedTempoFactor = 1.0 / 1.2;

float     failedNotes[] = {Gb1, Gb1};
uint32_t  failedBeats[] = {E,   E};
uint16_t  failedLength  = sizeof(failedNotes) / sizeof(failedNotes[0]);
float failedTempoFactor = 1.0 / 1.2;

float     startupNotes[] = {E5,C5,C6 };
uint32_t  startupBeats[] = {E, E, Q+E};
uint16_t  startupLength  = sizeof(startupNotes) / sizeof(startupNotes[0]);
float startupTempoFactor = 1.0 / 1.5;

#define SERVO_DATA_PIN      7
#define SERVO_ON_PIN        2
#define LED_PIN             13
#define LATCH_SWITCH_PIN    6
#define SPEAKER_PIN         3
#define RFID_ENABLE_PIN     4   // Connects to the RFID's ENABLE pin
#define RFID_RX_PIN         5   // Serial input (connects to the RFID's SOUT pin)
#define BUFSIZE             11  // Size of receive buffer (in bytes) (10-byte unique ID + null character)
#define RFID_TX_PIN         12  // Unused but needed to initialize serial connection

#define RFID_START          '\n'  // RFID Reader Start and Stop bytes
#define RFID_STOP           '\r'
char rfidData[BUFSIZE];  // Buffer for incoming data
char lastTag[BUFSIZE];
char* keyCard1 = "0F030389C6";
char* keyCard2 = "0F03038938";
char* keyFob1  = "04002139E2";
char* keyFob2  = "030043B424";

bool isLocked = false;
uint16_t readSuccess;
uint32_t lastRead  = 0;
uint16_t readDelay = 7000; // Milis that lock will wait between reading RFID. 
                           // Prevents servo toggling back before card is out of range.
bool startup = true;
// set up a new serial port
SoftwareSerial rfidSerial =  SoftwareSerial(RFID_RX_PIN, RFID_TX_PIN);

#define DEG_90        90    // 90 degrees for the servo
#define DEG_0         0     // 0 degrees for the servo
#define UNLOCK_ANGLE  65    // Angle required to unlock lever
#define LOCK_ANGLE    0     // Angle required to lock lever
#define DEG_90_PULSE  1475  // Pulse in microseconds for ~90 degrees
#define DEG_0_PULSE   750   // Pulse in microseconds for ~0 degrees
#define PULSE_WINDOW  20000 // Window for sending a command to servo in microseconds

void setup()
{
  Serial.begin(9600);

  pinMode(RFID_ENABLE_PIN,  OUTPUT);
  pinMode(RFID_RX_PIN,      INPUT);
  pinMode(LED_PIN,          OUTPUT);
  pinMode(LATCH_SWITCH_PIN, INPUT);
  pinMode(SPEAKER_PIN,      OUTPUT);
  pinMode(SERVO_ON_PIN,     OUTPUT);
  pinMode(SERVO_DATA_PIN,   INPUT);
  
  digitalWrite(RFID_ENABLE_PIN, HIGH);
  rfidSerial.begin(2400);
}

void loop()
{
  //Unlock on startup
  if(startup)
  {
    play_tune(startupNotes, startupBeats, startupLength, startupTempoFactor);
    reset_lock();
    startup = false;  
  }
  
  //digitalWrite(RFID_ENABLE_PIN, LOW);   //disable the RFID Reader
  memset(rfidData, '\0', BUFSIZE);
  if (rfidSerial.available() > 0 && rfidSerial.read() == RFID_START && (millis() - lastRead) > readDelay)
  {
    //Serial.println(millis() - lastRead);
    lastRead = millis();
    readSuccess = rfidSerial.readBytesUntil(RFID_STOP, rfidData, BUFSIZE);
    rfidSerial.flush();
    if (readSuccess != 0)
    {
      //Serial.print("Successful Read - ");
      Serial.println(rfidData);
    }
    else
    {
      //Serial.println("Error Reading From Serial");
    }
  }
  //digitalWrite(RFID_ENABLE_PIN, HIGH);   // enable the RFID Reader

  bool isConor    = strcmp(rfidData, keyCard1) == 0 || strcmp(rfidData, keyFob1) == 0 ? true : false;
  bool isMichelle = strcmp(rfidData, keyCard2) == 0 || strcmp(rfidData, keyFob2) == 0 ? true : false;

  if (isConor || isMichelle)
  {
    //Serial.println("Access Granted");
    if (isConor)
    {
      play_tune(conorNotes, conorBeats, conorLength, conorTempoFactor);
    }
    else if (isMichelle)
    {
      play_tune(michelleNotes, michelleBeats, michelleLength, michelleTempoFactor);
    }
    toggle_lock();
  }
  else if (rfidData[1] > 0)
  {
    //Serial.println("Access Denied");
    //play_tune(deniedNotes, deniedBeats, deniedLength, deniedTempoFactor);
  }
  isConor = false;
  isMichelle = false;
}

bool is_locked()
{
    return digitalRead(LATCH_SWITCH_PIN) == HIGH ? true : false;
}

void reset_lock()
{
    if(is_locked())
    {
      toggle_lock();
    }
}

// Moves the servo to the opposite angle it is currently at
// If lock was not moved to the other angle, a fail sound is played
void toggle_lock()
{
    bool isLocked = is_locked();
    if (isLocked)
    {
      move_servo(LOCK_ANGLE, UNLOCK_ANGLE);
    }
    else
    {
      move_servo(UNLOCK_ANGLE, LOCK_ANGLE);
    }
    if(isLocked == is_locked())
    {
       play_tune(failedNotes, failedBeats, failedLength, failedTempoFactor);
    }
}


void move_servo(int fromAngle, int targetAngle)
{
  int fromPulse     = map(fromAngle, DEG_0, DEG_90, DEG_0_PULSE, DEG_90_PULSE);
  int targetPulse   = map(targetAngle, DEG_0, DEG_90, DEG_0_PULSE, DEG_90_PULSE);
  
  digitalWrite(SERVO_ON_PIN, HIGH);
  pinMode(SERVO_DATA_PIN, OUTPUT);
  digitalWrite(SERVO_DATA_PIN, LOW);
  //delay(200);
  if(fromPulse <= targetPulse)
  {
    for(int pulse = fromPulse; pulse <= targetPulse; pulse+=10)
     {
       // Turn voltage high to start the period and pulse
       digitalWrite(SERVO_DATA_PIN, HIGH);
       delayMicroseconds(pulse);
       digitalWrite(SERVO_DATA_PIN, LOW);
       delayMicroseconds(PULSE_WINDOW-pulse);
       delay(15);
     }
  }
  else
  {
     for(int pulse =fromPulse; pulse >= targetPulse; pulse-=10)
     {
       // Turn voltage high to start the period and pulse
       digitalWrite(SERVO_DATA_PIN, HIGH);
       delayMicroseconds(pulse);
       digitalWrite(SERVO_DATA_PIN, LOW);
       delayMicroseconds(PULSE_WINDOW-pulse);
       delay(15);
     }
  }
  delay(250); // Wait for servo to overcome any resistance
  pinMode(SERVO_DATA_PIN, INPUT); // Prevent reverse current leakage from FET
  digitalWrite(SERVO_ON_PIN, LOW);
}

void beep(float noteFrequency, uint32_t noteDuration)
{
  tone(SPEAKER_PIN, noteFrequency, noteDuration);
  delay(noteDuration * 1.2);
  noTone(SPEAKER_PIN);
}

void play_tune(float notes[], uint32_t beats[], uint16_t numNotes, float tempoFactor)
{
  for (int i = 0; i < numNotes; i++)
  {
    if (notes[i] == REST)
    {
      delay(beats[i]*tempoFactor); // rest
    }
    else
    {
      beep(notes[i], beats[i]*tempoFactor);
    }
  }
}
